(defmodule sample-module
  (export all))

(defun my-sum (start stop)
  (let ((my-list (lists:seq start stop)))
    (* 2 (lists:foldl
          (lambda (n acc)
            (+ n acc))
          0 my-list))))

(defun demo ()
  (io:format "2 * (6 + 5 + 4 + 3 + 2 + 1) = ~p~n" (list (my-sum 1 6)))
  (io:format "2 * (60 + 59 + ...) = ~p~n" (list (my-sum 1 60)))
  (io:format "2 * (600 + 599 + ...) = ~p~n" (list (my-sum 1 600)))
  (io:format "2 * (6000 + 5999 + ...) = ~p~n" (list (my-sum 1 6000))))
